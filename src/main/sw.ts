// FCM

// importScripts("https://www.gstatic.com/firebasejs/9.8.0/firebase-app-compat.js");
// importScripts("https://www.gstatic.com/firebasejs/9.8.0/firebase-messaging-compat.js");
// // importScripts("https://www.gstatic.com/firebasejs/8.4.1/firebase-app.js");
// // importScripts("https://www.gstatic.com/firebasejs/8.4.1/firebase-messaging.js");
// (self as any).firebase.initializeApp({
//     apiKey: "AIzaSyCnrSMsBbiDAdO9FUNWsrDaGpoWSsZL8vA",
//     authDomain: "peryl-pwa.firebaseapp.com",
//     projectId: "peryl-pwa",
//     storageBucket: "peryl-pwa.appspot.com",
//     messagingSenderId: "109207901144",
//     appId: "1:109207901144:web:3d7f6ce04fee96790218ad"
// });
// // (self as any).firebase.messaging();
// const messaging = (self as any).firebase.messaging();
// console.log("FCM messaging", messaging);

// // If you would like to customize notifications that are received in the
// // background (Web app is closed or not in browser focus) then you should
// // implement this optional method.
// // Keep in mind that FCM will still show notification messages automatically
// // and you should use data messages for custom notifications.
// // For more info see:
// // https://firebase.google.com/docs/cloud-messaging/concept-options
// messaging.onBackgroundMessage(function(payload) {
//     console.log("[firebase-messaging-sw.js] Received background message ", payload);
//     // Customize notification here
//     const notificationTitle = "Background Message Title";
//     const notificationOptions = {
//       body: "Background Message body.",
//       icon: "/firebase-logo.png"
//     };
//     self.registration.showNotification(notificationTitle,
//         notificationOptions);
// });


// Pushy.me

// Import Pushy Service Worker 1.0.8
// importScripts("https://sdk.pushy.me/web/1.0.5/pushy-service-worker.js");
// importScripts("pushy-service-worker-1.0.5.js");
// importScripts("https://sdk.pushy.me/web/1.0.8/pushy-service-worker.js");
importScripts("pushy-service-worker-1.0.8.js");


// Notification

// self.addEventListener("notificationclick", e => {
//     // console.log("Notification click: ", e, (self as any).clients);
//     if ((e as any).notification.data.onAction) {
//         const url = (e as any).notification.data.onAction[(e as any).action];
//         url && (e as any).waitUntil((self as any).clients.openWindow(url));
//         (e as any).notification.close();
//     }
// });

// self.addEventListener("notificationclose", e => {
//     console.log("Notification close: ", e);
// });


// WorkBox
// https://developers.google.com/web/tools/workbox/guides/get-started
// https://codelabs.developers.google.com/codelabs/workbox-lab/
// https://developer.chrome.com/docs/workbox/modules/workbox-sw/

declare const importScripts: (script: string) => void;
declare const workbox: any;

importScripts("workbox-sw.js");
workbox.setConfig({
    modulePathPrefix: ""
    // debug: true
});

import { precacheAndRoute, matchPrecache } from "workbox-precaching";
import { setCatchHandler } from "workbox-routing";

// Ensure your build step is configured to include /offline.html as part of your precache manifest.
precacheAndRoute((self as any).__WB_MANIFEST);

// Catch routing errors, like if the user is offline
setCatchHandler(async ({ event }) => {
    // Return the precached offline page if a document is being requested
    if (event.request.destination === "document") {
        return matchPrecache("offline.html");
    }
    return Response.error();
});
