import { post } from "peryl/dist/http";

export class PushyMe {

    readonly deviceToken: string;

    constructor(appId: string,
        onRegister?: (deviceToken: string) => void,
        onData?: (data: any) => void
    ) {
        (window as any).Pushy.register({ appId })
            .then((deviceToken: string) => {
                console.log("Pushy device token: " + deviceToken);
                (this.deviceToken as any) = deviceToken;
                onRegister && onRegister(deviceToken);
                // Send the token to your backend server via an HTTP GET request
                // fetch("https://your.api.hostname/register/device?token=" + deviceToken);
                // Succeeded, optionally do something to alert the user
            })
            .catch((err: Error) => {
                console.error(err);
            });
        // Handle push notifications (only when web page is open)
        // (window as any).Pushy.setNotificationListener((data: any) =>  console.log("data: ", data));
        onData && (window as any).Pushy.setNotificationListener(onData);
    }

    push(apiKey: string, title: string, message: string, url?: string): void {
        post("https://api.pushy.me/push", { api_key: apiKey })
            .onError(e => console.error("Pushy send error", e))
            .onResponse(r => console.log("Pushy send response", r.getJson()))
            .send({
                to: this.deviceToken,
                data: {
                    title,
                    message,
                    url: url || location.href, // Opens when clicked
                    // image: "https://example.com/image.png" // Optional image
                    data: {
                        key: "value"
                    }
                }
            });
    }
}
